import logo from "./logo.svg";
import "./App.css";
import json from "./data/users.json";

// TODO: tarvittavat React-komponentit


function App() {
  const endpoint = "https://europe-west1-dev-edukamu.cloudfunctions.net/users";

  console.log(json.data);

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>


    </div>
  );
}

export default App;
